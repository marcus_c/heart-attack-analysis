# Heart Attack Analysis

A survival analysis of feature importance in the Worcester Heart Attack Study dataset - https://scikit-survival.readthedocs.io/en/latest/api/generated/sksurv.datasets.load_whas500.html#sksurv.datasets.load_whas500

## Installation

Developed and tested in python 3.7.4

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the required packages.

```bash
pip install -r requirements.txt
```
